<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: PUT");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');


// include database and object files
include_once '../config/database.php';
include_once '../model/user.php';

// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare user object
$existingUser = new User($db);
$existingUser->id = isset($_GET['id']) ? $_GET['id'] : die();
if ($_SERVER['REQUEST_METHOD'] != "PUT") {
    http_response_code(405);
    echo json_encode(
        array("message" => "Method not allowed.")
    );
    return;
}
if(!$existingUser->readOne()) {
    http_response_code(404);
    echo json_encode(
        array("message" => "No user found.")
    );
    die();
}

$data = json_decode(file_get_contents("php://input"));

$newUser = new User($db);
$newUser->id = $existingUser->id;
$newUser->username = $data->username;
$newUser->email = $data->email;

$usernameOrEmailInUser = $newUser->checkIfExists();
$userIsValid = $newUser->checkIfValid();

if ($usernameOrEmailInUser != null) {  
    http_response_code(400);
    echo json_encode(
        array("message" => $usernameOrEmailInUser)
    );
    return; 
}

if ($userIsValid != null) {
    http_response_code(400);
    echo json_encode(
        array("message" => $userIsValid)
    );
    return;
}

if ($newUser->update()) {
    echo json_encode(
        array("message" => "User updated!")
    );
    return;
} else {
    http_response_code(500);
    echo json_encode(
        array("message" => "Unable to update user!")
    );
}                                       
?>