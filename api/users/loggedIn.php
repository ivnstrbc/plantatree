<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../model/user.php';
include_once '../login.php';
 
// instantiate database and user object
$database = new Database();
$db = $database->getConnection();
 
 // prepare user object
$user = new User($db);
$userId = Login::isLoggedIn();
if (!$userId) {
    echo json_encode(
        array("username" => null, 
        "loggedin" => false)
    );
    return;
}

$user->id = $userId;

// query users
if(!$user->readOne()) {
    echo json_encode(
        array("username" => null, 
        "loggedin" => false, 
        "message"=>"No user found")
    );
    return;
} else {
    echo json_encode(
        array("username" => $user->username, 
        "loggedin" => true)
    );
    return;
}
?>