<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');


// include database and object files
include_once '../config/database.php';
include_once '../model/user.php';

// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare user object
$user = new User($db);
if ($_SERVER['REQUEST_METHOD'] != "POST") {
    http_response_code(405);
    echo json_encode(
        array("message" => "Method not allowed.")
    );
    return;
}

$data = json_decode(file_get_contents("php://input"));

$user->username = $data->username;
$user->email = $data->email;
$user->password = $data->password;

$userExists = $user->checkIfExists();
$userIsValid = $user->checkIfValid();

if ($userExists != null) {  
    http_response_code(400);
    echo json_encode(
        array("message" => $userExists)
    );
    return; 
}

if ($userIsValid != null) {
    http_response_code(400);
    echo json_encode(
        array("message" => $userIsValid)
    );
    return;
}
$userId = $user->create();
if ($userId != null) {
    echo json_encode(
        array("message" => "User created!", 
        "id" => $userId)
    );
    return;
} else {
    http_response_code(500);
    echo json_encode(
        array("message" => "Unable to create user!")
    );
}                                       
?>