<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');


// include database and object files
include_once '../config/database.php';
include_once '../model/user.php';

// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare location object
$user = new User($db);
if ($_SERVER['REQUEST_METHOD'] != "POST") {
    http_response_code(405);
    echo json_encode(
        array("message" => "Method not allowed.")
    );
    return;
}

$data = json_decode(file_get_contents("php://input"));

$user->username = $data->username;
$user->password = $data->password;

$loggedInError = $user->login();

if ($loggedInError!=null) {
    // http_response_code(400);
    echo json_encode(
        array("message" => $loggedInError)
    );
    return;

}

$token = $user->insertToken();
setcookie("PATID", $token, time() + 60 * 60 * 24 * 7, '/', NULL, NULL, TRUE);
setcookie("PATID_", '1', time() + 60 * 60 * 24 * 3, '/', NULL, NULL, TRUE);
http_response_code(200);
    echo json_encode(
        array("Token" => $token)
    );
return;
?>