<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
 
// include database and object files
include_once '../config/database.php';
include_once '../model/user.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare user object
$user = new User($db);
 
// set ID property of user to be edited
$user->id = isset($_GET['id']) ? $_GET['id'] : die();
 
// read the details of user to be edited
if ($user->readOne()) {
    // create array
    $user_arr = array(
        "id" =>  $user->id,
        "username" => $user->username,
        "email" => $user->email,
        "created_at" => $user->created_at
    );
    // make it json format
    print_r(json_encode($user_arr));
} else {
    http_response_code(404);
    echo json_encode(
        array("message" => "No user found.")
    );
    die();
}
?>