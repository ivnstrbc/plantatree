<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: DELETE");


// include database and object files
include_once '../config/database.php';
include_once '../model/user.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

if (!isset($_GET['id'])) {
    http_response_code(405);
    echo  json_encode(
        array("message" => "No id passed.")
    );
    die();
}
// prepare user object
$user = new User($db);
$user->id = isset($_GET['id']) ? $_GET['id'] : die();
if ($_SERVER['REQUEST_METHOD'] != "DELETE") {
    http_response_code(405);
    echo json_encode(
        array("message" => "Method not allowed.")
    );
    return;
}
if(!$user->readOne()) {
    http_response_code(404);
    echo json_encode(
        array("message" => "No user found.")
    );
    die();
}

if ($user->delete()) {
    echo json_encode(
        array("message" => "User deleted!")
    );
    return;
} else {
    http_response_code(500);
    echo json_encode(
        array("message" => "Unable to delete user!")
    );
}                                       
?>