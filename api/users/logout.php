<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header('Content-Type: application/json');

include_once '../model/loginToken.php';
include_once '../login.php';
include_once '../config/database.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

$userId = Login::isLoggedIn();
if (!$userId) {
    die("Not logged in.");
}

$data = json_decode(file_get_contents("php://input"));

if (isset($data->confirm)) {
    $loginToken = new LoginToken($db);
    $loginToken->user_id = $userId;
        
    if (isset($data->allDevices)) {
        $loginToken->deleteAll();
        setcookie('PATID', '1', time()-3600);
        setcookie('PATID_', '1', time()-3600);
        echo json_encode(
            array("message" => "User logged out!")
        );
        return;
    } else {
        if (isset($_COOKIE['PATID'])) {
            $loginToken->token = sha1($_COOKIE['PATID']);
            $loginToken->delete();
        }
        setcookie('PATID', '1', time()-3600);
        setcookie('PATID_', '1', time()-3600);
        echo json_encode(
            array("message" => "User logged out!")
        );
        return;
    }
} else {
    die('Not confirmed');
}
?>
