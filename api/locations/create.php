<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');


// include database and object files
include_once '../config/database.php';
include_once '../model/location.php';
include_once '../login.php';

// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare location object
$location = new Location($db);
if ($_SERVER['REQUEST_METHOD'] != "POST") {
    http_response_code(405);
    echo json_encode(
        array("message" => "Method not allowed.")
    );
    return;
}
$userId = Login::isLoggedIn();
if (!$userId) {
    die("Not logged in.");
}

$data = json_decode(file_get_contents("php://input"));

$location->latitude = $data->latitude;
$location->longitude = $data->longitude;
$location->created_by = $userId;

$locationExists = $location->checkIfExists();

if ($locationExists != null) {  
    http_response_code(400);
    echo json_encode(
        array("message" => $locationExists)
    );
    return; 
}
$id = $location->create();
if ($id != null) {
    echo json_encode(
        array("message" => "Location created!", 
        "id" => $id)
    );
    return;
} else {
    http_response_code(500);
    echo json_encode(
        array("message" => "Unable to create location!")
    );
}                                       
?>