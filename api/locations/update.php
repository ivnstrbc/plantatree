<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: PUT");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');


// include database and object files
include_once '../config/database.php';
include_once '../model/location.php';
include_once '../login.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

$userId = Login::isLoggedIn();
if (!$userId) {
    die("Not logged in.");
}
// prepare location object
$existingLocation = new Location($db);
$existingLocation->id = isset($_GET['id']) ? $_GET['id'] : die();
if ($_SERVER['REQUEST_METHOD'] != "PUT") {
    http_response_code(405);
    echo json_encode(
        array("message" => "Method not allowed.")
    );
    return;
}

if(!$existingLocation->readOne(false)) {
    http_response_code(404);
    echo json_encode(
        array("message" => "No location found.")
    );
    die();
}

$data = json_decode(file_get_contents("php://input"));

$newLocation = new Location($db);
$newLocation->id = $existingLocation->id;
$newLocation->latitude = $data->latitude;
$newLocation->longitude = $data->longitude;

$locationAlreadyExists = $newLocation->checkIfExists();

if ($locationAlreadyExists != null) {  
    http_response_code(400);
    echo json_encode(
        array("message" => $locationAlreadyExists)
    );
    return; 
}


if ($newLocation->update()) {
    echo json_encode(
        array("message" => "Location updated!")
    );
    return;
} else {
    http_response_code(500);
    echo json_encode(
        array("message" => "Unable to update location!")
    );
}                                       
?>