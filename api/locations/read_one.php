<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
 
// include database and object files
include_once '../config/database.php';
include_once '../model/location.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare location object
$location = new Location($db);
 
// set ID property of location to be edited
$location->id = isset($_GET['id']) ? $_GET['id'] : die();

// read the details of location to be edited
$location->readOne(true);

if ($location->latitude != null) {
    // create array
    $popup = createPopupHtml($location);
    $location_arr = array(
        "id" =>  $location->id,
        "name" => $location->name,
        "latitude" => $location->latitude,
        "longitude" => $location->longitude,
        "latin_name" => $location->latin_name, 
        "type" => $location->type,
        "images" => $location->images,
        "plant_id"=>$location->plant_id, 
        "created_by_username"=>$location->created_by_username,
        "markerHtml"=>$popup
    );
    // make it json format
    print_r(json_encode($location_arr));
} else {
    http_response_code(404);
    echo json_encode(
        array("message" => "No location found.")
    );
    die();
}

function createPopupHtml($location) {
    $baseUrl = 'https://www.studenti.famnit.upr.si/~89191154/plantatree/web/';
    $markerContent = '<div class="locationInfo">';
        if (isset($location->name)) {
            $imagesHtml = createImagesHtml($location->images);
            $markerContent .= $imagesHtml;
            $markerContent .= '<p class="species"> ' . $location->name;
            $markerContent .= '<br />(<i> ' . $location->latin_name . '</i>)</p>';
            $markerContent .= '<p>Plant added by <b>' .$location->created_by_username .'</b>';
            $markerContent .= '<br /> at '.$location->plant_created_at .'</p>';
            $markerContent .= '<a class="action-btn" onclick="openImageUploadForm('.$location->plant_id . ')">Add more photos!</a>'; 
        } else {
            $markerContent .= '<p>This could be location of your next plant!</p>';
            $markerContent .= '<a class ="action-btn" onclick="openCreatePlantForm('.$location->id .')">Choose your tree!</a>'; 
        }
        $markerContent .= '</div>'; 
        return $markerContent;
}

//Craetes slider gallery of images for map popup
function createImagesHtml($images) {
    $baseUrl = 'https://www.studenti.famnit.upr.si/~89191154/plantatree/web/';
    $content = '<div class="slideshow-container">';
    $count = sizeof($images);
    if ($count == 0) {
        return '<div class="noimage">No images to show</div>';
    }

    for ($i = 0; $i < sizeof($images); $i++) {
        $image = $images[$i];
            $imageUrl = $baseUrl . $image["image_path"];
            $order = $i+1;
            $orderText = $order ." / ".$count;
            $caption = $image['created_at'];
            // Full-width images with number and caption text -->
            $content .= '<div class="mySlides fade">';
            $content .= ' <div class="numbertext">'.$orderText.'</div>';
            $content .= '<div class="image"><img src="'.$imageUrl.'" ></div>';
            $content .= '<div class="text">'.$caption.'</div></div>';
    }
    $content .= '
        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>
        </div>
        <br>  
        <div style="text-align:center">';
    
    for ($i = 0; $i < sizeof($images); $i++) {
        $order = $i+1;
        $content .= '<span class="dot" onclick="currentSlide('.$order.')"></span>';
    }
     
    $content .= '</div>';
    return $content;
}
 
?>