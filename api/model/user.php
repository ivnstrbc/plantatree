<?php
include_once '../model/loginToken.php';

class User {
    
    private $conn;
	private $table_name = "user";
	
	// 	object properties
	public $id;
	public $username;
    public $password;
    public $email;
    public $user_role;

    // constructor with $db as database connection
	public function __construct($db){
		$this->conn = $db;
    }

    function login() {
        $query = "SELECT u.username, u.password FROM ". $this->table_name." u  WHERE
			u.username = ?"; 
		$stmt = $this->conn->prepare( $query );	
		$stmt->bindParam(1, $this->username);
		$stmt->execute();
		$num = $stmt->rowCount();
        if ($num > 0) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if ( password_verify($this->password, $row['password'])) {
                return null;
            } else {
                return "Username or password are not correct.";    
            }
        } else {
            return "Username or password are not correct.";
        }
    }

    function insertToken() {
        $query = "SELECT u.id FROM ". $this->table_name." u  WHERE
			u.username = ?"; 
		$stmt = $this->conn->prepare( $query );	
		$stmt->bindParam(1, $this->username);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $user_id = $row['id'];
        $cstrong = True;
        $token = bin2hex(openssl_random_pseudo_bytes(64, $cstrong));
        
        $loginToken = new LoginToken($this->conn);
        $loginToken->token = sha1($token);
        $loginToken->user_id = $user_id;
        $loginToken->insert();
        return $token;               
    }
    
    function create() {
        $query = "INSERT INTO ".$this->table_name." SET
        username=:username, password=:password, email=:email";
        $stmt = $this->conn->prepare($query);
        
         // bind values
        $stmt->bindParam(":username", $this->username);
        $stmt->bindParam(":password", password_hash($this->password, PASSWORD_BCRYPT));
        $stmt->bindParam(":email", $this->email);
        // execute query
        if($stmt->execute()){
            return $this->conn->lastInsertId();
        } else {
            return null;
        }
    }

    function update() {
        $query = "UPDATE ".$this->table_name." SET
        username=:username, email=:email WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        
        // bind values
        $stmt->bindParam(":username", $this->username);
        $stmt->bindParam(":email", $this->email);
        $stmt->bindParam(":id", $this->id);
        // execute query
        if($stmt->execute()){
            return true;
        } else {
            return false;
        }
    }

    function delete() {
        $query = "DELETE FROM ". $this->table_name." WHERE
			id = ?"; 
		$stmt = $this->conn->prepare( $query );	
		$stmt->bindParam(1, $this->id);
        // execute query

        if($stmt->execute()){
            return true;
        } else {
            // die(mysqli_error($this->conn))
            return false;
        }
    }
    
    // read all users
	function read() {
        $query = "SELECT
            u.id, u.username, u.email,  
            u.user_role
        FROM
            " . $this->table_name . " u ";
        $stmt = $this->conn->prepare($query);
        $stmt->execute(); 
        return $stmt;
    }

    function checkIfExists() {
        $updating = $this->id != null;
        //Check for username
        $query = "SELECT username FROM ". $this->table_name." u  
        WHERE u.username = ?";
        // Do not compare with current username if id is set
        // Used when updating
        if ($updating) {
            $query .= " AND u.id != ?";
        } 
		$stmt = $this->conn->prepare( $query );	
		$stmt->bindParam(1, $this->username);
        if ($updating) {
            $stmt->bindParam(2, $this->id);
        }
        $stmt->execute();
		$num = $stmt->rowCount();
        if ($num > 0) {
            return "Username already exists!";
        }         

        //check email
        $query = "SELECT email FROM ". $this->table_name." u  WHERE
        u.email = ?"; 
        // Do not compare with current username if id is set
        // Used when updating
        if ($updating) {
            $query .= " AND u.id != ?";
        }
        $stmt = $this->conn->prepare( $query );	
        $stmt->bindParam(1, $this->email);
        if ($updating) {
            $stmt->bindParam(2, $this->id);
        }
        $stmt->execute();
        $num = $stmt->rowCount();
        if ($num > 0) {
            return "Email is already in use!";
        } 
        return null;
    }

    function checkIfValid() {
        if (strlen($this->username) < 3 || strlen($this->username) > 32 || !preg_match('/[a-zA-Z0-9_]+/', $this->username)) {
            return "Invalid username!";
        }
        if (isset($this->password) && (strlen($this->password) < 6 || strlen($this->password) > 60)) {
            return "Invalid password!";
        }
        
        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            return "Invalid email!";
        }
        return null;
    }

    // get single location with species, plant info and it's images
	function readOne() {	
		// query to read single record
		$query = "SELECT
		u.id, u.username, u.email, 
		u.user_role
		FROM
                " . $this->table_name . " u
            WHERE
                u.id = ?
            LIMIT
				0,1";
				
		// prepare query statement
		$stmt = $this->conn->prepare( $query );

		// bind id of product to be updated
		$stmt->bindParam(1, $this->id);
		
		// execute query
		$stmt->execute();
        $num = $stmt->rowCount();
        if ($num < 1) {
            return false;
        } 
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
    
		// set values to object properties
		$this->username = $row['username'];
		$this->email = $row['email'];
        $this->user_role = $row['user_role'];
        return true;
	}
}
?>