<?php
class PlantImages {
	
	// 	database connection and table name
	private $conn;
	private $table_name = "plant_image";
	
	// 	object properties
	public $id;
	public $plant_id;
	public $image_name;
    public $image_path;
	public $created_at;
	public $created_by;
	public $images;

	// constructor with $db as database connection
	public function __construct($db){
		$this->conn = $db;
	}
	
	// read plant images for this plant_id
	function read(){
		$query = "SELECT
                s.id, s.plant_id, s.image_name, 
                s.image_path, s.created_at, s.created_by
                FROM " . $this->table_name . " s ";
        if (isset($this->plant_id)) {
            $query .= " WHERE s.plant_id = :plant_id";
        } 
        $stmt = $this->conn->prepare($query);
        if (isset($this->plant_id)) {
            $stmt->bindParam(":plant_id", $this->plant_id);
        }
        $stmt->execute(); 
        $num = $stmt->rowCount();
        // check if more than 0 record found
        if($num>0){
            // images array
            $images=array();
            // retrieve our table contents
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                array_push($images, $row);
            }
            return $images;
        } else {
            return array();
        }
    }

    function insert() {
        $query = "INSERT INTO ". $this->table_name." SET
        plant_id=:plant_id, image_path=:image_path, 
        image_name=:image_name, created_by=:created_by";
        $stmt = $this->conn->prepare($query);
        
         // bind values
        $stmt->bindParam(":plant_id", $this->plant_id);
        $stmt->bindParam(":image_name", $this->image_name);
        $stmt->bindParam(":image_path", $this->image_path);
        $stmt->bindParam(":created_by", $this->created_by);
        
        if ($stmt->execute()) {
            return $this->conn->lastInsertId();
        } else {
            return null;
        }

    }

    function delete() {
        $query = "DELETE FROM ". $this->table_name." WHERE
			id = ?"; 
		$stmt = $this->conn->prepare( $query );	
		$stmt->bindParam(1, $this->id);
        // execute query

        if($stmt->execute()){
            return true;
        } else {
            // die(mysqli_error($this->conn))
            return false;
        }
    }

    // get single plant image
	function readOne() {	
        // query to read single record
		$query = "SELECT id, plant_id, image_name, 
        image_path, created_at, created_by
		FROM
                " . $this->table_name . " u
            WHERE
                id = ?
            LIMIT
				0,1";
				
		// prepare query statement
		$stmt = $this->conn->prepare( $query );

		// bind id of product to be updated
		$stmt->bindParam(1, $this->id);
		
		// execute query
		$stmt->execute();
        $num = $stmt->rowCount();
        if ($num < 1) {
            return false;
        } 
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
    
		// set values to object properties
		$this->plant_id = $row['plant_id'];
        $this->image_name = $row['image_name'];
        $this->image_path = $row['image_path'];
        $this->created_by = $row['created_by'];
        $this->created_at = $row['created_at'];
        return true;
	}

}
?>