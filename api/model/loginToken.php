<?php
class LoginToken {

    private $conn;
	private $table_name = "login_tokens";
	
	// 	object properties
	public $id;
	public $user_id;
    public $token;

    // constructor with $db as database connection
	public function __construct($db){
		$this->conn = $db;
    }

    function insert() {
        $query = "INSERT INTO ". $this->table_name." SET
        user_id=:userId, token=:token";
        $stmt = $this->conn->prepare($query);
        
         // bind values
        $stmt->bindParam(":userId", $this->user_id);
        $stmt->bindParam(":token", $this->token);
        $stmt->execute();
    }

    function delete() {
        $query = "DELETE FROM ". $this->table_name." WHERE
         token=:token";
        $stmt = $this->conn->prepare($query);
        
        // bind values
        $stmt->bindParam(":token", $this->token);
        if($stmt->execute()){
            return true;
        } else {
            return false;
        }
    }

    function deleteAll() {
        $query = "DELETE FROM ". $this->table_name." WHERE
         user_id=?";
        $stmt = $this->conn->prepare($query);
        
        // bind values
        $stmt->bindParam(1, $this->user_id);
        $stmt->execute();
        if($stmt->execute()){
            return true;
        } else {
            return false;
        }
    }

    function getUserId() {
        $query = "SELECT user_id FROM ". $this->table_name." WHERE
        token = ?"; 
        $stmt = $this->conn->prepare( $query );	
        $stmt->bindParam(1, $this->token);
        $stmt->execute();
        $num = $stmt->rowCount();
        if ($num > 0) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->user_id = $row['user_id'];
            return $this->user_id;
        } else {
            return false;
        }
    }
}
?>