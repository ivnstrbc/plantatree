<?php
class Species {
	
	// 	database connection and table name
	private $conn;
	private $table_name = "species";
	
	// 	object properties
	public $id;
	public $name;
	public $latin_name;
    public $type;
    public $shape;
    public $color;
	public $created_by;

	// constructor with $db as database connection
	public function __construct($db){
		$this->conn = $db;
	}
	
	// read locations with species and plant info
	function read(){
		    $query = "SELECT
                s.id, s.name, s.latin_name, s.type, 
                s.shape, s.color, s.created_by
            FROM
                " . $this->table_name . " s ";
        $stmt = $this->conn->prepare($query);
		$stmt->execute(); 
        return $stmt;
	}

	function create() {
        $query = "INSERT INTO ".$this->table_name." SET
        name=:name, latin_name=:latin_name, created_by=:created_by,
        type=:type, shape=:shape, color=:color";
        $stmt = $this->conn->prepare($query);
        
         // bind values
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":latin_name", $this->latin_name);
        $stmt->bindParam(":created_by", $this->created_by);
        $stmt->bindParam(":type", $this->type);
        $stmt->bindParam(":shape", $this->shape);
        $stmt->bindParam(":color", $this->color);
        
        // execute query
        if($stmt->execute()){
            return true;
        } else {
            return false;
        }
	}
	
	function checkIfExists() {
        $updating = $this->id != null;
        //Check for username
        $query = "SELECT * FROM ". $this->table_name." s  
        WHERE (s.name = :name OR s.latin_name=:latin_name) ";
        // Do not compare with current username if id is set
        // Used when updating
        if ($updating) {
            $query .= " AND s.id != :id";
		} 
		$stmt = $this->conn->prepare( $query );	
		
		// bind values
		 $stmt->bindParam(":name", $this->name);
		 $stmt->bindParam(":latin_name", $this->latin_name);
		if ($updating) {
            $stmt->bindParam(":id", $this->id);
        }
        $stmt->execute();
		$num = $stmt->rowCount();
        if ($num > 0) {
            return "Species with same name or latin name already exists!";
		}
		return null;
    }
    
    function checkIfValid() {
        if (!isset($this->name)) {
            return "Invalid name!";
        }
        if (!isset($this->latin_name)) {
            return "Invalid latin name!";
        }
        if (!isset($this->type)) {
            return "Invalid type!";
        }
        return null;
    }

	function update() {
        $query = "UPDATE ".$this->table_name." SET
        name=:name, latin_name=:latin_name,
        type=:type, shape=:shape, color=:color
        WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        
        // bind values
        $stmt->bindParam(":id", $this->id);
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":latin_name", $this->latin_name);
        $stmt->bindParam(":type", $this->type);
        $stmt->bindParam(":shape", $this->shape);
        $stmt->bindParam(":color", $this->color);
        // execute query
        if($stmt->execute()){
            return true;
        } else {
            return false;
        }
    }

    function delete() {
        $query = "DELETE FROM ". $this->table_name." WHERE
			id = ?"; 
		$stmt = $this->conn->prepare( $query );	
		$stmt->bindParam(1, $this->id);
        // execute query

        if($stmt->execute()){
            return true;
        } else {
            return false;
        }
    }
    
    // get single species
	function readOne() {	
		// query to read single record
		$query = "SELECT
		s.id, s.name, s.latin_name, 
        s.type, s.shape, s.color,
        s.created_by 
		FROM
                " . $this->table_name . " s
            WHERE
                s.id = ?
            LIMIT
				0,1";
				
		// prepare query statement
		$stmt = $this->conn->prepare( $query );

		// bind id of product to be updated
		$stmt->bindParam(1, $this->id);
		
		// execute query
		$stmt->execute();
        $num = $stmt->rowCount();
        if ($num < 1) {
            return false;
        } 
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
    
		// set values to object properties
		$this->id = $row['id'];
        $this->name = $row['name'];
        $this->latin_name = $row['latin_name'];
        $this->type = $row['type'];
        $this->shape = $row['shape'];
        $this->color = $row['color'];
        $this->created_by = $row['created_by'];
        
        return true;
	}
}
