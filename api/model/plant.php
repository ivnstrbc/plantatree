<?php
include_once '../model/plantImages.php';
include_once '../model/location.php';

class Plant {
	
	// 	database connection and table name
	private $conn;
	private $table_name = "plant";
	
	// 	object properties
	public $id;
	public $species_id;
	public $locations_id;
	public $created_at;
	public $created_by;
    public $images;
    public $name;
    public $latin_name;
    public $type;
    public $latitude;
    public $longitude;

	// constructor with $db as database connection
	public function __construct($db){
		$this->conn = $db;
	}
	
	// read plants with species and locations
	function read(){
		$query = "SELECT
            p.id, p.species_id, p.location_id, 
            p.created_at, p.created_by, 
            l.latitude, l.longitude, 
            s.name, s.latin_name, s.type 
            FROM
                    " . $this->table_name . " p
             LEFT JOIN location l ON p.location_id = l.id 
             LEFT JOIN species s ON s.id = p.species_id";
        $stmt = $this->conn->prepare($query);
		$stmt->execute(); 
        return $stmt;
	}

	function create() {
        if ($this->locations_id == null) {
            $location = new Location($this->conn);
            $location->latitude = $this->latitude;
            $location->longitude = $this->longitude;
            $location->created_by = $this->created_by;
            $this->locations_id = $location->create();
        } else {
            $location = new Location($this->conn);
            $location->id = $this->locations_id;
            $location->readOne(false);
            $location->update();
        }
        $query = "INSERT INTO ".$this->table_name." SET
        species_id=:species_id, location_id=:location_id,
        created_by=:created_by";
        $stmt = $this->conn->prepare($query);
        
         // bind values
        $stmt->bindParam(":species_id", $this->species_id);
        $stmt->bindParam(":location_id", $this->locations_id);
        $stmt->bindParam(":created_by", $this->created_by);
        
        // execute query
        if($stmt->execute()){
            return $this->conn->lastInsertId();
        } else {
            return null;
        }
	}
	
	function checkIfExists() {
        if ($this->locations_id == null) {
            return null;
        }
        $updating = $this->id != null;
        //Check for username
        $query = "SELECT * FROM ". $this->table_name." s  
        WHERE (s.species_id = :species_id AND s.location_id=:location_id) ";
        // Do not compare with current username if id is set
        // Used when updating
        if ($updating) {
            $query .= " AND s.id != :id";
		} 
		$stmt = $this->conn->prepare( $query );	
		
		// bind values
		 $stmt->bindParam(":species_id", $this->species_id);
		 $stmt->bindParam(":location_id", $this->locations_id);
		if ($updating) {
            $stmt->bindParam(":id", $this->id);
        }
        $stmt->execute();
		$num = $stmt->rowCount();
        if ($num > 0) {
            return "Same plant on this location already exists!";
		}
		return null;
    }

    function checkIfValid() {
        if (!isset($this->species_id)) {
            return "Species is not set";
        }
        if ($this->locations_id == null && ($this->latitude == null || $this->longitude == null)) {
            return "Location is not set.";
        }
        return null;
    }

	function update() {
        $query = "UPDATE ".$this->table_name." SET
        species_id=:species_id, location_id=:location_id";
        $stmt = $this->conn->prepare($query);
        
         // bind values
        $stmt->bindParam(":species_id", $this->species_id);
        $stmt->bindParam(":location_id", $this->locations_id);
        
        // execute query
        if($stmt->execute()){
            return true;
        } else {
            return false;
        }
    }

    function delete() {
        $query = "DELETE FROM ". $this->table_name." WHERE
			id = ?"; 
		$stmt = $this->conn->prepare( $query );	
		$stmt->bindParam(1, $this->id);
        // execute query

        if($stmt->execute()){
            return true;
        } else {
            return false;
        }
    }
    
    // get single species
	function readOne($loadImages) {	
		// query to read single record
		$query = "SELECT
		p.id, p.species_id, p.location_id, 
        p.created_at, p.created_by, 
        l.latitude, l.longitude, 
        s.name, s.latin_name, s.type 
		FROM
                " . $this->table_name . " p
         LEFT JOIN location l ON p.location_id = l.id 
         LEFT JOIN species s ON s.id = p.species_id
        WHERE 
        p.id = ?
            LIMIT
				0,1";
				
		// prepare query statement
		$stmt = $this->conn->prepare( $query );

		// bind id of product to be updated
		$stmt->bindParam(1, $this->id);
		
		// execute query
		$stmt->execute();
        $num = $stmt->rowCount();
        if ($num < 1) {
            return false;
        } 
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
    
		// set values to object properties
        $this->id = $row['id'];
        $this->species_id = $row['species_id'];
        $this->locations_id = $row['location_id'];
        $this->species_id = $row['species_id'];
        
        $this->created_at = $row['created_at'];
        $this->created_by = $row['created_by'];
        $this->name = $row['name'];
        $this->latin_name = $row['latin_name'];
        $this->type = $row['type'];
        $this->latitude = $row['latitude'];
        $this->longitude = $row['longitude'];
        if ($loadImages) {
            $plantImages = new PlantImages($this->conn);
            $plantImages->plant_id = $this->id;
            $this->images = $plantImages->read();
        }
        
        return true;
	}
}
