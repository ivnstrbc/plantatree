<?php
include_once '../model/plantImages.php';
include_once '../model/user.php';

class Location {
	
	// 	database connection and table name
	private $conn;
	private $table_name = "location";
	
	// 	object properties
	public $id;
	public $latitude;
	public $longitude;
	public $created_by;
	public $name;
	public $latin_name;
	public $type;
	public $plant_created_at;
	public $plant_id;
	public $images;
	public $created_by_username;

	// constructor with $db as database connection
	public function __construct($db){
		$this->conn = $db;
	}
	
	// read locations with species and plant info
	function read(){
		    $query = "SELECT
                l.id, l.latitude, l.longitude, 
                p.created_at, 
                s.name, s.latin_name, s.type
            FROM
                " . $this->table_name . " l 
            LEFT JOIN plant p ON p.location_id = l.id 
            LEFT JOIN species s ON s.id = p.species_id";
        $stmt = $this->conn->prepare($query);
		$stmt->execute(); 
        return $stmt;
	}

	function create() {
        $query = "INSERT INTO ".$this->table_name." SET
		latitude=:latitude, longitude=:longitude, 
		created_by=:created_by";
        $stmt = $this->conn->prepare($query);
        
         // bind values
        $stmt->bindParam(":latitude", $this->latitude);
        $stmt->bindParam(":longitude", $this->longitude);
		$stmt->bindParam(":created_by", $this->created_by);
        // execute query
        if($stmt->execute()){
            return $this->conn->lastInsertId();
        } else {
            return null;
        }
	}
	
	function checkIfExists() {
        $updating = $this->id != null;
        //Check for username
        $query = "SELECT * FROM ". $this->table_name." l  
        WHERE l.latitude = :latitude AND l.longitude=:longitude";
        // Do not compare with current username if id is set
        // Used when updating
        if ($updating) {
            $query .= " AND u.id != :id";
		} 
		$stmt = $this->conn->prepare( $query );	
		
		// bind values
		 $stmt->bindParam(":latitude", $this->latitude);
		 $stmt->bindParam(":longitude", $this->longitude);
		if ($updating) {
            $stmt->bindParam(":id", $this->id);
        }
        $stmt->execute();
		$num = $stmt->rowCount();
        if ($num > 0) {
            return "Location with same latitude and longitude already exists!";
		}
		return null;
	}

	function update() {
        $query = "UPDATE ".$this->table_name." SET
		latitude=:latitude, longitude=:longitude WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        
        // bind values
        $stmt->bindParam(":latitude", $this->latitude);
		$stmt->bindParam(":longitude", $this->longitude);
        
        $stmt->bindParam(":id", $this->id);
        // execute query
        if($stmt->execute()){
            return true;
        } else {
            return false;
        }
    }

    function delete() {
        $query = "DELETE FROM ". $this->table_name." WHERE
			id = ?"; 
		$stmt = $this->conn->prepare( $query );	
		$stmt->bindParam(1, $this->id);
        // execute query

        if($stmt->execute()){
            return true;
        } else {
            return false;
        }
	}
	
	// get single location with species, plant info and it's images
	function readOne($loadPlantImages) {	
		// query to read single record
		$query = "SELECT
		l.id, l.latitude, l.longitude, l.created_by,
		p.created_at as plant_created_at, p.id as plant_id,
		s.name, s.latin_name, s.type
		FROM
                " . $this->table_name . " l 
            LEFT JOIN plant p ON p.location_id = l.id 
            LEFT JOIN species s ON s.id = p.species_id
            WHERE
                l.id = ?
            LIMIT
				0,1";
				
		// prepare query statement
		$stmt = $this->conn->prepare( $query );

		// bind id of product to be updated
		$stmt->bindParam(1, $this->id);
		
		// execute query
		$stmt->execute();
		$num = $stmt->rowCount();
        if ($num < 1) {
            return false;
        }
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->name = $row['name'];
		$this->latitude = $row['latitude'];
		$this->longitude = $row['longitude'];
		$this->created_by = $row['created_by'];
		$this->plant_created_at = $row['plant_created_at'];
		$this->latin_name = $row['latin_name'];
		$this->type = $row['type'];
		$this->plant_id = $row['plant_id'];
		if (!$loadPlantImages) {
			return true;
		}
		// get images
		if ($this->plant_id != null) {
			$plantImages = new PlantImages($this->conn);
            $plantImages->plant_id = $this->plant_id;
			$this->images = $plantImages->read();
		} else {
			$this->images = array();
		}

		//get created_by user's username
		if ($this->created_by != null) {
			$user = new User($this->conn);
            $user->id = $this->created_by;
			$user->readOne();
			$this->created_by_username = $user->username;
		} else {
			$this->created_by_username = "Unknown";
		}
		return true;
	}
}
