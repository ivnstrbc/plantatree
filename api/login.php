<?php
// include database and object files
include_once '../config/database.php';
include_once '../model/loginToken.php';


// get database connection

class Login {
        
    public static function isLoggedIn() {
        if (isset($_COOKIE['PATID'])) {
            
            $database = new Database();
            $db = $database->getConnection();
            $loginToken = new LoginToken($db);
            $loginToken->token = sha1($_COOKIE['PATID']);
            $userId = $loginToken->getUserId();
            if ($userId) {
                if (isset($_COOKIE['PATID_'])) {
                    return $userId;
                } else {
                    //refresh token
                    $cstrong = True;
                    $token = bin2hex(openssl_random_pseudo_bytes(64, $cstrong));      
                    $newLoginToken = new LoginToken($db);
                    $newLoginToken->user_id = $userId;
                    $newLoginToken->token = $token;
                    $newLoginToken->insert();

                    //Delete old token
                    $loginToken->delete();

                    setcookie("PATID", $token, time() + 60 * 60 * 24 * 7, '/', NULL, NULL, TRUE);
                    setcookie("PATID_", '1', time() + 60 * 60 * 24 * 3, '/', NULL, NULL, TRUE);

                    return $userId;
                }
            }
        }
        //By default return false
        return false;
    }
}

?>
