<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');


// include database and object files
include_once '../config/database.php';
include_once '../model/species.php';
include_once '../login.php';
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare species object
$species = new Species($db);
if ($_SERVER['REQUEST_METHOD'] != "POST") {
    http_response_code(405);
    echo json_encode(
        array("message" => "Method not allowed.")
    );
    return;
}
$userId = Login::isLoggedIn();
if (!$userId) {
    die("Not logged in.");
}

$data = json_decode(file_get_contents("php://input"));

$species->name = $data->name;
$species->latin_name = $data->latin_name;
$species->type = $data->type;
$species->shape = isset($data->shape) ? $data->shape : null;;
$species->color = isset($data->color) ? $data->color : null;
$species->created_by = $userId;

$speciesExists = $species->checkIfExists();

if ($speciesExists != null) {  
    http_response_code(400);
    echo json_encode(
        array("message" => $speciesExists)
    );
    return; 
}

$speciesValid = $species->checkIfValid();

if ($speciesValid != null) {  
    http_response_code(400);
    echo json_encode(
        array("message" => $speciesValid)
    );
    return; 
}

if ($species->create()) {
    echo json_encode(
        array("message" => "Species created!")
    );
    return;
} else {
    http_response_code(500);
    echo json_encode(
        array("message" => "Unable to create species!")
    );
}                                       
?>