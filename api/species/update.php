<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: PUT");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');


// include database and object files
include_once '../config/database.php';
include_once '../model/species.php';
include_once '../login.php';

// get database connection
$database = new Database();
$db = $database->getConnection();
 
$userId = Login::isLoggedIn();
if (!$userId) {
    die("Not logged in.");
}

// prepare species object
$existingSpecies = new Species($db);
$existingSpecies->id = isset($_GET['id']) ? $_GET['id'] : die();
if ($_SERVER['REQUEST_METHOD'] != "PUT") {
    http_response_code(405);
    echo json_encode(
        array("message" => "Method not allowed.")
    );
    return;
}
if(!$existingSpecies->readOne()) {
    http_response_code(404);
    echo json_encode(
        array("message" => "No species found.")
    );
    die();
}

$data = json_decode(file_get_contents("php://input"));

$newSpecies = new Species($db);
$newSpecies->id = $existingSpecies->id;
$newSpecies->name = $data->name;
$newSpecies->latin_name = $data->latin_name;
$newSpecies->type = $data->type;
$newSpecies->shape = isset($data->shape) ? $data->shape : null;;
$newSpecies->color = isset($data->color) ? $data->color : null;
$newSpecies->created_by = $userId;


$speciesExists = $newSpecies->checkIfExists();
$speciesValid = $newSpecies->checkIfValid();

if ($speciesExists != null) {  
    http_response_code(400);
    echo json_encode(
        array("message" => $speciesExists)
    );
    return; 
}

if ($speciesValid != null) {  
    http_response_code(400);
    echo json_encode(
        array("message" => $speciesValid)
    );
    return; 
}

if ($newSpecies->update()) {
    echo json_encode(
        array("message" => "Species updated!")
    );
    return;
} else {
    http_response_code(500);
    echo json_encode(
        array("message" => "Unable to update species!")
    );
}                                       
?>