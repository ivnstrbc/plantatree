<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
 
// include database and object files
include_once '../config/database.php';
include_once '../model/species.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare species object
$species = new Species($db);
 
// set ID property of species to be edited
$species->id = isset($_GET['id']) ? $_GET['id'] : die();
 
// read the details of species
if ($species->readOne()) {
    // create array
    $species_arr = array(
        "id" =>  $species->id,
        "name" => $species->name,
        "latin_name" => $species->latin_name,
        "type" => $species->type,
        "shape" => $species->shape,
        "color" => $species->color,
        "created_by" => $species->created_by
    );
    // make it json format
    print_r(json_encode($species_arr));
} else {
    http_response_code(404);
    echo json_encode(
        array("message" => "No species found.")
    );
    die();
}
 

?>