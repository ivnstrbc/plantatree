<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');


// include database and object files
include_once '../config/database.php';
include_once '../model/plant.php';
include_once '../login.php';
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare plant object
$plant = new Plant($db);
if ($_SERVER['REQUEST_METHOD'] != "POST") {
    http_response_code(405);
    echo json_encode(
        array("message" => "Method not allowed.")
    );
    return;
}
$userId = Login::isLoggedIn();
if (!$userId) {
    die("Not logged in.");
}

$data = json_decode(file_get_contents("php://input"));

$plant->species_id = $data->species_id;

if (isset($data->locations_id)) {
    $plant->locations_id = $data->locations_id;
} else {
    $plant->latitude = $data->latitude;
    $plant->longitude = $data->longitude;
}
$plant->created_by = $userId;
$plantExists = $plant->checkIfExists();

if ($plantExists != null) {  
    http_response_code(400);
    echo json_encode(
        array("message" => $plantExists)
    );
    return; 
}

$plantValid = $plant->checkIfValid();

if ($plantValid != null) {  
    http_response_code(400);
    echo json_encode(
        array("message" => $plantValid)
    );
    return; 
}
$plantId = $plant->create(); 
if ($plantId != null) {
    echo json_encode(
        array("message" => "Plant created!", 
        "id" => $plantId)
    );
    return;
} else {
    http_response_code(500);
    echo json_encode(
        array("message" => "Unable to create plant!")
    );
}                                       
?>