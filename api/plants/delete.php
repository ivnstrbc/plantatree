<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: DELETE");

// include database and object files
include_once '../config/database.php';
include_once '../model/plant.php';
include_once '../login.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

$userId = Login::isLoggedIn();
if (!$userId) {
    die("Not logged in.");
}

if (!isset($_GET['id'])) {
    http_response_code(405);
    echo  json_encode(
        array("message" => "No id passed.")
    );
    die();
}
// prepare plant object
$plant = new Plant($db);
$plant->id = isset($_GET['id']) ? $_GET['id'] : die();
if ($_SERVER['REQUEST_METHOD'] != "DELETE") {
    http_response_code(405);
    echo json_encode(
        array("message" => "Method not allowed.")
    );
    return;
}
if(!$plant->readOne(false)) {
    http_response_code(404);
    echo json_encode(
        array("message" => "No plant found.")
    );
    die();
}

if ($plant->delete()) {
    echo json_encode(
        array("message" => "Plant deleted!")
    );
    return;
} else {
    http_response_code(500);
    echo json_encode(
        array("message" => "Unable to delete plant!")
    );
}                                       
?>