<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../model/plant.php';

// instantiate database and plant object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$plant = new Plant($db);
 
// query plants
$stmt = $plant->read();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // plants array
    $plants_arr=array();
    $plants_arr["records"]=array();
 
    // retrieve our table contents
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        array_push($plants_arr["records"], $row);
    }
 
    echo json_encode($plants_arr);
}
 
else{
    echo json_encode(
        array("message" => "No plants found.")
    );
}
?>