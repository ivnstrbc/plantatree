<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");

// include database and object files
include_once '../config/database.php';
include_once '../model/plantImages.php';
include_once '../login.php';
// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare plant images object
$plantImages = new PlantImages($db);

$plantImages->plant_id = isset($_GET['id']) ? $_GET['id'] : die();

if ($_SERVER['REQUEST_METHOD'] != "POST") {
    http_response_code(405);
    echo json_encode(
        array("message" => "Method not allowed.")
    );
    return;
}
$userId = Login::isLoggedIn();
if (!$userId) {
    die("Not logged in.");
}

$currentDir = getcwd();
$uploadDirectory = "/web/images/";

$errors = []; // Store all foreseen and unforseen errors here

$fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions
$date = new DateTime();
$fileName = $_FILES['file']['name'];
$fileSize = $_FILES['file']['size'];
$fileTmpName  = $_FILES['file']['tmp_name'];
$fileType = $_FILES['file']['type'];
$fileExtension = strtolower(end(explode('.',$fileName)));

$uploadPath = dirname(dirname($currentDir)) . $uploadDirectory . $date->getTimestamp() .".". $fileExtension; 

if (! in_array($fileExtension,$fileExtensions)) {
    $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
}

if ($fileSize > 10000000) {
    $errors[] = "This file is more than 10MB. Sorry, it has to be less than or equal to 2MB";
}
if (!empty($errors)) {
    http_response_code(400);
    echo json_encode(
        array("message" => $errors[0])
    );
    die();
}

$didUpload = move_uploaded_file($fileTmpName, $uploadPath);
if (!$didUpload) {
    http_response_code(400);
    $message = "Was not able to upload file to ".$uploadPath;
    echo json_encode(
        array("message" => $message)
    );
    die();
}


$plantImages->image_path = "images/".$date->getTimestamp() .".". $fileExtension;
$image_name = isset($_POST['image_name']) ? $_POST['image_name'] : basename($fileName);
$plantImages->image_name = $image_name;
$plantImages->created_by = $userId;
$imageId = $plantImages->insert();
if ($imageId != null) {
    echo json_encode(
        array("message" => "File uploaded!", 
        "id" => $imageId)
    );
} else {
    http_response_code(400);
    echo json_encode(
        array("message" => "Unable to create db entry ")
    );
    die();
}
                                      
?>