<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
 
// include database and object files
include_once '../config/database.php';
include_once '../model/plant.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare plant object
$plant = new Plant($db);
 
// set ID property of plant to be edited
$plant->id = isset($_GET['id']) ? $_GET['id'] : die();

// read the details of plant
if ($plant->readOne(true)) {
    // create array
    $plant_arr = array(
        "id" =>  $plant->id,
        "name" => $plant->name,
        "latin_name" => $plant->latin_name,
        "type" => $plant->type,
        "species_id" => $plant->species_id,
        "locations_id" => $plant->locations_id,
        "latitude" => $plant->latitude,
        "longitude" => $plant->longitude,
        "created_at" => $plant->created_at, 
        "created_by" => $plant->created_by,
        "images" => $plant->images
    );
    // make it json format
    print_r(json_encode($plant_arr));
} else {
    http_response_code(404);
    echo json_encode(
        array("message" => "No plant found.")
    );
    die();
}
 

?>