// Variables 
var map;
// Location markers shown on map
var markers = [];
// Expanded location marker
var richMarker;
// Base URL
var baseurl = 'https://www.studenti.famnit.upr.si/~89191154/plantatree/'; 
var imageAssetsURL = baseurl + 'web/assets/images/'; 
var rootAPI = baseurl + 'api/';

// Tree icon names
var icons = {
    noleaves: {
      icon: imageAssetsURL + 'tree-icon-grey.png'
  },
  leaves: {
      icon: imageAssetsURL + 'tree-icon-green.png'
  }
};

// Pure AJAX get function that expects JSON in return
function ajax_get(url, callback) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            try {
                var data = JSON.parse(xmlhttp.responseText);
            } catch(err) {
                console.log(err.message + " in " + xmlhttp.responseText);
                return;
            }
            callback(data);
        }
    };

    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

// Pure AJAX post function that expects JSON in return
function ajax_post(url, data, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
        try {
            var data = JSON.parse(xhr.responseText);
            callback(data);
        } catch(err) {
            console.log(err.message + " in " + xhr.responseText);
            return;
        }

    };
    xhr.send(data);
}

function ajax_upload(url, file, fileName, callback) {
    var fd = new FormData();
    fd.append("file", file);
    fd.append("image_name", fileName);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    xhr.upload.onprogress = function(e)
    {
        var percentComplete = Math.ceil((e.loaded / e.total) * 100);
    };
    xhr.onload = function() 
    {
        try {  
            var data = JSON.parse(xhr.responseText);
            callback(data);
        } catch(err) {
            console.log(err.message + " in " + xhr.responseText);
            return;
        }
    }
    xhr.send(fd);
}

function openNavigationMenu() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}

var openedMarker;
// Initializes map with all locations read and centered in hardcoded lat and lng
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: 44.770037, lng: 17.182759},
      zoom: 14
  });
    var bounds = new google.maps.LatLngBounds();
    ajax_get(rootAPI + 'locations/read.php', function(data) {
        var locations = data["records"];
        
        for (i = 0; i < locations.length; i++) { 
            var location = locations[i]; 
            var lat = parseFloat(locations[i]["latitude"]);
            var lng = parseFloat(locations[i]["longitude"]);
            var id = parseInt(locations[i]["id"]);
            
            var isBysy = false;
            var icon = icons["noleaves"].icon;
            var content = locations[i]["name"];
            if (location["name"] !== "undefined" && location["name"] != null) {
                isBusy = true;
                icon = icons["leaves"].icon;
            }
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                icon: icon,
                map: map, 
                locationId: id
            });
            
            //extend the bounds to include each marker's position
            bounds.extend(marker.position);
            google.maps.event.addListener(marker, 'click', function() {
                openInfoWindow(this);
            });
        }
    });
}

var infoWindow;
var slideIndex = 1;
function openInfoWindow(marker) {
    openedMarker = marker;
    if (typeof(infoWindow) !== "undefined") {
        infoWindow.close();
    }
    ajax_get(rootAPI + 'locations/read_one.php?id='+marker.locationId, function(data) {
        var markerContent = data["markerHtml"];
        infoWindow = new google.maps.InfoWindow({
            content: markerContent
        });
        infoWindow.open(map, marker);
        if (data["name"] !== "undefined" && data["name"] != null) {
            if (data["images"].length > 0) {
                slideIndex = 1;
                setTimeout(function() { showSlides(slideIndex); }, 100);
                
            }
        }
    });

}

function openMarker(marker) {
    // close previous opened
    if (typeof(richMarker) !== "undefined") {
        richMarker.setMap(null);
    }
    
    ajax_get(rootAPI + 'locations/read_one.php?id='+marker.locationId, function(data) {  
        markerContent = data["markerHtml"]; 
        richMarker = new RichMarker({
            map: map, 
            position: marker.position,
            draggable: false, 
            flat: true,
            anchor: RichMarkerPosition.BOTTOM,
            content: markerContent
        });
        if (data["name"] !== "undefined" && data["name"] != null) {
            if (data["images"].length > 0) {
                slideIndex = 1;
                setTimeout(function() { showSlides(slideIndex); }, 100);
                
            }
        }
    }); 
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    if (slides.length === 0) { 
        console.log("Empty slides...");
        return; 
    }    
    if (n > slides.length) {slideIndex = 1}    
    if (n < 1) {slideIndex = slides.length}
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";  
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "block";  
    dots[slideIndex-1].className += " active";
}
  

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}


function openLoginForm() {
    document.getElementById("loginForm").style.display = "block";
    document.getElementById('modalbg').style.display = "block";
}

function closeLoginForm() {
    document.getElementById("loginForm").style.display = "none";
    document.getElementById('modalbg').style.display = "none";
}

function openRegisterForm() {
    document.getElementById("registerForm").style.display = "block";
    document.getElementById('modalbg').style.display = "block";
}

function closeRegisterForm() {
    document.getElementById("registerForm").style.display = "none";
    document.getElementById('modalbg').style.display = "none";
}

function openImageUploadForm(plantId) {
    if (document.getElementById("username").innerHTML == "") {
        alert("You have to login first");
        openLoginForm(); 
        return;
    }
    document.getElementById('imageUpload-id').value = plantId;
    document.getElementById("plantImageUploadForm").style.display = "block";
    document.getElementById('modalbg').style.display = "block";
}

function closeImageUploadForm() {
    document.getElementById("plantImageUploadForm").style.display = "none";
    document.getElementById('modalbg').style.display = "none";
    if (infoWindow != undefined ) {
        infoWindow.close();
    }
    if (openedMarker != undefined) {
        openInfoWindow(openedMarker);
    }

}

function openCreatePlantForm(locationId) {
    if (document.getElementById("username").innerHTML == "") {
        alert("You have to login first");
        openLoginForm(); 
        return;
    }
    document.getElementById('createPlant-locationId').value = locationId;
    document.getElementById("createPlantForm").style.display = "block";
    document.getElementById('modalbg').style.display = "block";
    reloadSpeciesDropdown();
}

function closeCreatePlantForm() {
    document.getElementById("createPlantForm").style.display = "none";
    document.getElementById('modalbg').style.display = "none";
}
function reloadNavigation() {
    ajax_get(rootAPI + 'users/loggedIn.php', function(data) {
        var loggedInElements = document.getElementsByClassName("loggedin");
        var loggedOutElements = document.getElementsByClassName("loggedout");
        
        if (!data["loggedin"]) {
            for(i = 0; i < loggedInElements.length; i++) {
                loggedInElements[i].style.display = "none";
            }
            for(i = 0; i < loggedOutElements.length; i++) {
                loggedOutElements[i].style.display = "block";
            }
            document.getElementById("username").innerHTML = "";
        } else {
            var username = data["username"];
            if (username == undefined) {
                username = "Unknown";
            }
            for(i = 0; i < loggedInElements.length; i++) {
                loggedInElements[i].style.display = "block";
            }
            for(i = 0; i < loggedOutElements.length; i++) {
                loggedOutElements[i].style.display = "none";
            }
        
            document.getElementById("username").innerHTML = "<i>Logged in as: </i>" +username;
        }
    });
}

function submitLoginForm() {
  var username = document.getElementById("login-username").value;
  var password = document.getElementById("login-psw").value;
  var data = JSON.stringify({"username": username, "password": password});
  ajax_post(rootAPI + 'users/auth.php', data, function(data) {
    if (data["Token"] != undefined) {
        document.getElementById("loginerror").innerHTML = "";
        closeLoginForm();
        reloadNavigation();
    } else {
        var message = (data["message"] != undefined) ? data["message"] : "Unexpected error! Please try again.";
        document.getElementById("login-psw").value = "";
        document.getElementById("loginerror").innerHTML = message;
    }  
});
}

function submitRegisterForm() {
    var username = document.getElementById("register-username").value;
    var password = document.getElementById("register-psw").value;
    var email = document.getElementById("register-email").value;
    var data = JSON.stringify({"username": username, "password": password, "email": email });
    ajax_post(rootAPI + 'users/register.php', data, function(data) {
        if (data["id"] != undefined) {
            document.getElementById("registererror").innerHTML = "";
            closeRegisterForm();
            openLoginForm();
        } else {
            var message = (data["message"] != undefined) ? data["message"] : "Unexpected error! Please try again.";
            document.getElementById("register-psw").value = "";
            document.getElementById("registererror").innerHTML = message;
        }  
    });
}

function submitImageUploadForm() {
    var imageName = document.getElementById("imageUpload-imageName").value;
    var file = document.getElementById('imageUpload-file').files[0];
    var plantId = document.getElementById('imageUpload-id').value;
    ajax_upload(rootAPI + 'plants/uploadImage.php?id='+plantId, file, imageName, function(data) {
      if (data["id"] != undefined) {
          document.getElementById("imageUploaderror").innerHTML = "";
          document.getElementById("imageUpload-imageName").value;
          document.getElementById('imageUpload-file').files = null;
          document.getElementById('imageUpload-id').value = "";
    
          closeImageUploadForm();
          //TODO reload
    } else {
        var message = (data["message"] != undefined) ? data["message"] : "Unexpected error! Please try again.";
        document.getElementById("imageUploaderror").innerHTML = message;
    }  
  });
}

function submitCreatePlantForm() {
    var locationId = document.getElementById("createPlant-locationId").value;
    var speciesId = document.getElementById("createPlant-speciesId").value;
    var data = JSON.stringify({"locations_id": locationId, "species_id": speciesId });
    ajax_post(rootAPI + 'plants/create.php', data, function(data) {
      if (data["id"] != undefined) {
          document.getElementById("createPlanterror").innerHTML = "";
          document.getElementById("createPlant-locationId").value = "";
          document.getElementById("createPlant-speciesId").value = null;
          closeCreatePlantForm();
    } else {
        var message = (data["message"] != undefined) ? data["message"] : "Unexpected error! Please try again.";
        document.getElementById("register-psw").value = "";
        document.getElementById("createPlanterror").innerHTML = message;
    }  
  });
}

function logout() {
    var data = JSON.stringify({"confirm": true, "alldevices": true});
    ajax_post(rootAPI + 'users/logout.php', data, function(data) {
      reloadNavigation();
    });
}

function reloadSpeciesDropdown() {
    var html = "";
    ajax_get(rootAPI + 'species/read.php', function(data) {
        var species = data["records"];
        for (i = 0; i < species.length; i++) { 
            var id = parseFloat(species[i]["id"]);
            var name = species[i]["name"];
            html += "<option value='" + id +"'>" + name + "</option>";   
        }
        document.getElementById("createPlant-speciesId").innerHTML = html;
    });
}
reloadNavigation();
